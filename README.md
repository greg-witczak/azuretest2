1) Install Azure CLI 2 from https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest
2) Open PowerShell and run following commands:
```
$ az login
$ az account set --subscription <subscription Id>
```
3) Open your project folder in same console session and run:
```
mvn clean install azure-webapp:deploy
```
4) Visit :
https://gwitczak-azuretest2-win-h2.azurewebsites.net/hello?name=Grzes and 
https://gwitczak-azuretest2-win-h2.azurewebsites.net/notes